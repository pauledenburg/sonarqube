# Set up captainduckduck
To set up SonarQube on your captainduckduck server you need to do some things:
1. Make the SonarQube Docker image ready for captainduckduck
1. Set up the database on captainduckduck

Sources
* SonarQube files taken from https://github.com/SonarSource/docker-sonarqube/tree/master/6.7.6-community
* generated captain-definition file on https://githubsaturn.github.io/dockerfile-to-captain/



## Create captain-definition file
Create the file `captain-definition` with the following contents.

```
{
  "schemaVersion": 1,
  "dockerfileLines": [
    "FROM sonarqube:7.5-community"
  ]
}
```

### Put everything in git
With captainduckduck you can only deploy projects which are on git. 

So put everything in git.

```
git init
git add --all .
git commit -m "initial commit"
git push
```

## Database Container
SonarQube needs to store its data somewhere. We'll use a postgres container for that purpose (as SonarQube suggests).

Create database container in captainduckduck:
* new container
* one-click app > choose 'postgres'
* fill in the info (mind the info you supplied above!)
  * container name: sq-db
  * docker tag: 10.1
  * postgres root username: sonar
  * postgres root password: v07IGCFCF83Z95NX
  * postgres default db: sonarqube 

Then, when it's created, open up port 5432
## SonarQube on CaptianDuckDuck
Now we'll create a new app on your captainduckduck server.

In CaptainDuckDuck, create a new app and make sure to check the option `Has Persistent Data`.

Provide the following env vars for the gitlabrunner container.
* SONARQUBE_JDBC_USERNAME=sonar
* SONARQUBE_JDBC_PASSWORD=v07IGCFCF83Z95NX
* SONARQUBE_JDBC_URL=jdbc:postgresql://srv-captain--sq-db:5432/sonarqube

Ports to open in captainduckduck (public:local):
* 9000:9000
* 9092:9092

Persistent directories:
- /opt/sonarqube/conf:sonarqube-conf
- /opt/sonarqube/data:sonarqube-data
- /opt/sonarqube/extensions:sonarqube-extensions
- /opt/sonarqube/lib/bundled-plugins:sonarqube-bundled-plugins



## Deploy Sonarqube

```
captainduckduck deploy
```
